package com.nikitaivanov.ucladiningassistant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by Garrett on 11/8/2015.
 */

// changed this from android.app.Fragment to android.support.v4.app.Fragment. this might cause problems.
public class HoursFragment extends Fragment {

    public static final String KEY_MEALPERIOD = "MealPeriod";

    public enum MEALPERIOD {
        BREAKFAST("Breakfast"),
        LUNCH("Lunch"),
        DINNER("Dinner"),
        LATENIGHT("Late Night");

        public String text;

        MEALPERIOD(String text) {
            this.text = text;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_listview, container, false);

        Bundle bundle = getArguments();
        int value_meal_bundle = bundle.getInt(KEY_MEALPERIOD);

        MEALPERIOD mealperiod = null;

        for (MEALPERIOD m : MEALPERIOD.values()) {
            if (m.ordinal() == value_meal_bundle) {
                mealperiod = m;
                break; // might cause problems
            }
        }

        /*TextView heading = (TextView)view.findViewById(R.id.heading_mealperiod);
        heading.setText(mealperiod.text);*/

        ListView listView = (ListView) view.findViewById(R.id.fragment_listview);
        listView.setDivider(null);
        listView.setDividerHeight(0);
        //listView.setEnabled(false);

        HoursAdapter hoursAdapter = new HoursAdapter(getActivity(), MainActivity.getHoursScraperList(), mealperiod);
        listView.setAdapter(hoursAdapter);

        return view;
    }

    public static HoursFragment newInstance(int position) {
        HoursFragment hoursFragment = new HoursFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_MEALPERIOD, position);
        hoursFragment.setArguments(bundle);
        return hoursFragment;
    }
}

