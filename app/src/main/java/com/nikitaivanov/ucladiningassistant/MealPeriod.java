package com.nikitaivanov.ucladiningassistant;

import java.util.ArrayList;
import java.util.Vector;

/**
 * A MealPeriod object belongs to a given DiningHall and contains the kitchens for that DiningHall
 * at the given mealPeriod.
 */
public class MealPeriod {

    int meal;
    private String[] hours;
    public ArrayList<Kitchen> kitchens;

    final static int BREAKFAST = 0;
    final static int LUNCH = 1;
    final static int DINNER = 2;
    final static int LATENIGHT = 3;

    public MealPeriod(int meal){
        this.meal = meal;
        this.hours = new String[2];
        kitchens = new ArrayList<Kitchen>();
        /*for(int i = 0; i < 4; i++){
            kitchens.add(new Kitchen());
        }*/
    }

    public Kitchen getKitchen(int i){
        return kitchens.get(i);
    }

    public ArrayList<Kitchen> getKitchens(){
        return kitchens;
    }

    public void setHours(String[] hours){
        this.hours = hours;
    }

    public String[] getHours(){
        return hours;
    }

    public String getKitchenName(int i){
        return kitchens.get(i).getName();
    }

    public ArrayList<Dish> getKitchenDishes(int i){
        return kitchens.get(i).getDishes();
    }
}
