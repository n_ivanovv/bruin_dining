package com.nikitaivanov.ucladiningassistant;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;


/**
 * Created by Nikita on 11/4/2015.
 */
public class VerticalPagerAdapter extends FragmentStatePagerAdapter{

    private int mealPeriod;
    private static int currentDiningHall;

    public VerticalPagerAdapter(FragmentManager fm, int mealPeriod) {

        super(fm);
        this.mealPeriod = mealPeriod;
    }

    @Override
    public Fragment getItem(int position) {


        int diningHall = MainActivity.getDiningHallIndex(position);

        Fragment fragment = DiningHallFragment.newInstance(diningHall, mealPeriod);
        Log.d("NI", "Creating a fragment for dining hall #" + MainActivity.dh[diningHall].getName() + " for period " + mealPeriod);

        return fragment;
    }

    public static int getCurrentDiningHall(){
       return currentDiningHall;
    }


    @Override
    public int getCount() {
        return MainActivity.NUM_MENU_DINING_HALLS;
    }

    @Override
    public CharSequence getPageTitle(int position){
        int diningHall = MainActivity.getDiningHallIndex(position);
        return MainActivity.dh[diningHall].getName();
    }
}
