package com.nikitaivanov.ucladiningassistant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Garrett on 11/11/2015.
 */
public class HoursParentFragment extends Fragment {

    HoursFragmentPagerAdapter hoursFragmentPagerAdapter;
    private static ViewPager viewPager;
    private static int currentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        currentView = getArguments().getInt("currentView");


        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.meal_period_parent_layout, container, false);

        hoursFragmentPagerAdapter = new HoursFragmentPagerAdapter(getFragmentManager());

        viewPager = (ViewPager) view.findViewById(R.id.mealPeriodPager);
        viewPager.setAdapter(hoursFragmentPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                MainActivity.getTabLayout().getTabAt(position).select();
                currentView = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(currentView);
        return view;
    }

    public static void setCurrentView(int view){
        currentView = view;
        viewPager.setCurrentItem(currentView);
    }


    public static ViewPager getPager(){
        return viewPager;
    }

    public static HoursParentFragment newInstance(int currentView){
        HoursParentFragment fragment = new HoursParentFragment();
        Bundle args = new Bundle();
        args.putInt("currentView", currentView);
        fragment.setArguments(args);
        return fragment;

    }
}
