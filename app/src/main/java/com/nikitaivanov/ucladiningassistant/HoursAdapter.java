package com.nikitaivanov.ucladiningassistant;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Garrett on 11/8/2015.
 */
public class HoursAdapter extends BaseAdapter implements ListAdapter {

    private Context context;
    private List<HoursScraper> hoursScraperList;
    private HoursFragment.MEALPERIOD mealperiod;

    public HoursAdapter(Context context, List<HoursScraper> hoursScraperList, HoursFragment.MEALPERIOD mealPeriod) {
        this.context = context;
        this.hoursScraperList = hoursScraperList;
        this.mealperiod = mealPeriod;
    }

    @Override
    public int getCount() {
        return hoursScraperList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.meal_period_layout, null);

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.dininghall_listview_container);
        if (position % 2 == 0) {
            linearLayout.setBackground(parent.getResources().getDrawable(R.drawable.rounded_edges));
        }


        TextView dininghall = (TextView) view.findViewById(R.id.mealperiod_dininghall);
        TextView hours = (TextView) view.findViewById(R.id.mealperiod_hours);
        TextView comments = (TextView) view.findViewById(R.id.mealperiod_comments);

        switch (mealperiod) {
            case BREAKFAST: {
                dininghall.setText(hoursScraperList.get(position).getName());
                hours.setText(hoursScraperList.get(position).getBreakHrs()[0]);
                comments.setText(hoursScraperList.get(position).getBreakHrs()[1]);
                break;
            }
            case LUNCH: {
                dininghall.setText(hoursScraperList.get(position).getName());
                hours.setText(hoursScraperList.get(position).getLunchHrs()[0]);
                comments.setText(hoursScraperList.get(position).getLunchHrs()[1]);
                break;
            }
            case DINNER: {
                dininghall.setText(hoursScraperList.get(position).getName());
                hours.setText(hoursScraperList.get(position).getDinnerHrs()[0]);
                comments.setText(hoursScraperList.get(position).getDinnerHrs()[1]);
                break;
            }
            case LATENIGHT: {
                dininghall.setText(hoursScraperList.get(position).getName());
                hours.setText(hoursScraperList.get(position).getLateNightHrs()[0]);
                comments.setText(hoursScraperList.get(position).getLateNightHrs()[1]);
                break;
            }
            default:
                Log.d("GK Consulting", "Something went wrong with your Switch");
        }

        return view;
    }

    @Override
    public long getItemId(int position) {
        return -1;
    }

    @Override
    public Object getItem(int position) {
        return hoursScraperList.get(position);
    }
}
