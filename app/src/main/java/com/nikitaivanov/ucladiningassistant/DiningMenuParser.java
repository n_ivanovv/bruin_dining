package com.nikitaivanov.ucladiningassistant;


import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.joda.time.LocalDateTime;



/**
 * Created by Nikita Ivanov on 11/2/2015
 * Parses the UCLA Dining website menus and places the information within the array of DiningHall objects
 * stored by the MainActivity
 */
public class DiningMenuParser {

    /*
        Parses the UCLA dining website by breaking it down into subsections based on the meal Period
        and the halls which each meal period is broken down into
     */
    public static void parse() throws IOException {
        // Validate.isTrue(args.length == 1, "usage: supply url to fetch");

        //Get the date information, which is used to construct the URL to be scraped
        Calendar currentDate = Calendar.getInstance();
        int month = currentDate.get(Calendar.MONTH);
        int day = currentDate.get(Calendar.DAY_OF_MONTH);
        int year = currentDate.get(Calendar.YEAR);


        //String url = "http://menu.ha.ucla.edu/foodpro/default.asp?date=" month + "%2F" + day + "%2F" + year;
        //Builds the URL for the current day's menus
        StringBuilder urlBuilder = new StringBuilder("http://menu.ha.ucla.edu/foodpro/default.asp?date=");
        urlBuilder.append(++month);
        urlBuilder.append("%2F");
        urlBuilder.append(day);
        urlBuilder.append("%2F");
        urlBuilder.append(year);

        String url = urlBuilder.toString();

        //For testing
        //String url = "http://menu.ha.ucla.edu/foodpro/default.asp?date=12%2F2%2F2015";

        Log.d("NI", "Parsing the following: " + url);

        //Creates new HTML document based on the url
        Document doc = Jsoup.connect(url).get();

        //Each meal section is denoted by the .menucontent identifier -> select these sections
        Elements mealTables = doc.select(".menucontent");

        int dayOfWeek = currentDate.get(Calendar.DAY_OF_WEEK);
        boolean weekend = ((dayOfWeek == Calendar.SATURDAY) || (dayOfWeek == Calendar.SUNDAY));
        switch(mealTables.size()){
            case 0:
                break;
            case 1:
                setupHalls(mealTables, MealPeriod.DINNER, weekend);
                break;
            case 2:
                setupHalls(mealTables, MealPeriod.LUNCH, weekend);
                setupHalls(mealTables, MealPeriod.DINNER, weekend);
                break;
            case 3:
                setupHalls(mealTables, MealPeriod.BREAKFAST, weekend);
                setupHalls(mealTables, MealPeriod.LUNCH, weekend);
                setupHalls(mealTables, MealPeriod.DINNER, weekend);
                break;
            default:
                break;
        }

        /*if (mealTables.size() != 0) {
            //Scrape based on the day of the week (since weekends only have two meals)
            int dayOfWeek = currentDate.get(Calendar.DAY_OF_WEEK);
            boolean weekend = ((dayOfWeek == Calendar.SATURDAY) || (dayOfWeek == Calendar.SUNDAY));
            //Sets up the halls for each of the meal periods
            /*if (!weekend && mealTables.size() == 3) {
                setupHalls(mealTables, MealPeriod.BREAKFAST, weekend);
                setupHalls(mealTables, MealPeriod.LUNCH, weekend);
                setupHalls(mealTables, MealPeriod.DINNER, weekend);
            }
            else if(weekend && mealTables.size() == 2){
                setupHalls(mealTables, MealPeriod.LUNCH, weekend);
                setupHalls(mealTables, MealPeriod.DINNER, weekend);
            }

        }*/
    }

    /*
        Breaks down the mealTables into mealPeriods which are then parsed on the basis of each dining hall
        @param mealTables the various mealTables to be parsed
        @param mealPeriod the current meal period (BREAKFAST, LUNCH, DINNER, LATENIGHT)
        @param weekend true if current date is a weekend, which is necessary for calculations
            related to indexing of the tables
     */
    private static void setupHalls(Elements mealTables, int mealPeriod, boolean weekend) {
        //If weekend, shift mealPeriod back since there are only two meal periods displayed
        int tableSize = mealTables.size();
        int shiftedMealPeriod = tableSize - (3 - mealPeriod);
        //int shiftedMealPeriod = weekend ? mealPeriod - 1 : mealPeriod;

        //Divide menu contents amongst the different dining halls
        //Each cell of the mealTables structure denotes the menu for a single dining hall in a single
        //meal period
        Element mealTable = mealTables.get(shiftedMealPeriod);
        Elements mealTableRows = mealTable.select(".menugridtable");

        try {
            //If typical grid formatting, parse as usual, dividing each meal section into its four dining halls
            if(mealTableRows.select(".menugridcell").size() == mealTableRows.select(".menugridcell_last").size()){
                Elements mealTableTopLeft = mealTableRows.get(0).select(".menugridcell");
                Elements mealTableTopRight = mealTableRows.get(0).select(".menugridcell_last");
                Elements mealTableBottomLeft = null;
                Elements mealTableBottomRight = null;

                if (mealPeriod != MealPeriod.BREAKFAST) {
                    mealTableBottomLeft = mealTableRows.get(1).select(".menugridcell");
                    mealTableBottomRight = mealTableRows.get(1).select(".menugridcell_last");
                }

                if (mealPeriod == MealPeriod.BREAKFAST) {
                    setupKitchens(mealTableTopLeft, DiningHall.DENEVE, mealPeriod);
                    setupKitchens(mealTableTopRight, DiningHall.BPLATE, mealPeriod);

                } else {
                    setupKitchens(mealTableTopLeft, DiningHall.COVEL, mealPeriod);
                    setupKitchens(mealTableTopRight, DiningHall.DENEVE, mealPeriod);
                    setupKitchens(mealTableBottomLeft, DiningHall.FEAST, mealPeriod);
                    setupKitchens(mealTableBottomRight, DiningHall.BPLATE, mealPeriod);
                }
            }
            //If weird weekend, three dining hall formatting occurs
            else{
                Elements mealTableLeftCols = mealTable.select(".menugridcell");
                Elements mealTableRightCol = mealTable.select(".menugridcell_last");
                Elements mealTableLeftCol = new Elements();
                Elements mealTableCenterCol = new Elements();
                for(int i = 0; i < mealTableLeftCols.size(); i++){
                    if(i % 2 == 0){
                        mealTableLeftCol.add(mealTableLeftCols.get(i));
                    }
                    else{
                        mealTableCenterCol.add(mealTableLeftCols.get(i));
                    }
                }

                setupKitchens(mealTableLeftCol, DiningHall.COVEL, mealPeriod);
                setupKitchens(mealTableCenterCol, DiningHall.DENEVE, mealPeriod);
                setupKitchens(mealTableRightCol, DiningHall.BPLATE, mealPeriod);
            }




        } catch(Exception e){
            //If an exception of some kind is thrown (due to changes in formatting of dining website
            //or other error causing behavior, record that an error occured in MainActivity to
            //ensure that the app does not crash, but the user is notified of misparsing
            MainActivity.setParsingStatus(true);
        }
    }

    /*
        Sets the names for each of the kitchens of Dining Hall and adds the dishes said kitchen serves
        @param tablePortion the portion of the mealTable parsed
        @param diningHall the index of the dining hall for which the kitchens are set
        @param mealPeriod the meal period for which the kitchens are set
     */
    private static void setupKitchens(Elements tablePortion, int diningHall, int mealPeriod) {
        int i = 0;
        Log.d("NIS", "Setting up DH: " + diningHall);
        for (Element cell : tablePortion) {
            if(diningHall == DiningHall.DENEVE){
                Log.d("NIS", "Setting up DeNeve Kitchen");
            }

            //Add new kitchen
            MainActivity.dh[diningHall].getMealPeriod(mealPeriod).getKitchens().add(new Kitchen());

            //Set Kitchen name
            MainActivity.dh[diningHall].getMealPeriod(mealPeriod).getKitchen(i).setName(cell.select(".category5").text());

            //Generate set of meals
            Elements meals = cell.select(".level5");

            //Add each meal
            for (Element meal : meals) {
                Dish dish = new Dish(meal.text(), diningHall, mealPeriod);
                MainActivity.dh[diningHall].getMealPeriod(mealPeriod).getKitchen(i).addDish(dish);
                MainActivity.todaysDishesSet.add(dish);
            }
            //Increment for next kitchen
            i++;
        }
    }
}



