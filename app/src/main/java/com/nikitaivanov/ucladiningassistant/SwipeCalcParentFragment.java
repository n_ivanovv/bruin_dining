package com.nikitaivanov.ucladiningassistant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Nikita on 11/25/2015.
 */
public class SwipeCalcParentFragment extends Fragment{
    SwipeCalcAdapter swipeCalcAdapter;
    private static ViewPager viewPager;
    private static int defaultSwipeIndex;

    @Override
    public void onCreate(Bundle savedInstanceState) {


        defaultSwipeIndex = getArguments().getInt("defaultSwipeIndex");

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.swipecalc_parent_layout, container, false);

        swipeCalcAdapter = new SwipeCalcAdapter(getFragmentManager());

        viewPager = (ViewPager) view.findViewById(R.id.swipeCalcPager);
        viewPager.setAdapter(swipeCalcAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                MainActivity.getTabLayout().getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setCurrentItem(defaultSwipeIndex);
        Log.d("NI", "Selecting plan at index: " + defaultSwipeIndex);
        return view;
    }

    @Override
    public void onStart() {

        super.onStart();
    }

    public static ViewPager getPager(){
        return viewPager;
    }

    public static SwipeCalcParentFragment newInstance(int defaultSwipeIndex){
        SwipeCalcParentFragment fragment = new SwipeCalcParentFragment();
        Bundle args = new Bundle();
        args.putInt("defaultSwipeIndex", defaultSwipeIndex);
        fragment.setArguments(args);
        return fragment;
    }
}

