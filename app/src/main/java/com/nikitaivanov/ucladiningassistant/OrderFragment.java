package com.nikitaivanov.ucladiningassistant;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * Fragment used to manipulate the order of the DiningHall objects displayed according to
 * user preferences
 */
public class OrderFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private String mActivityTitle;
    private OnFragmentInteractionListener mListener;
    private View myFragmentView;

    private Spinner first_slot_spinner;
    private Spinner second_slot_spinner;
    private Spinner third_slot_spinner;
    private Spinner fourth_slot_spinner;

    private boolean savedPref = false;

    private int[] diningHallIndexes = new int[] {DiningHall.DENEVE, DiningHall.BPLATE, DiningHall.COVEL, DiningHall.FEAST};
    private static int currentDiningHall;
    private int mealPeriod;

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public OrderFragment() {
        // Required empty public constructor

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (getArguments() != null) {
            mealPeriod = getArguments().getInt("mealPeriod");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.fragment_order, container, false);

        first_slot_spinner = (Spinner)myFragmentView.findViewById(R.id.first_slot_spinner);
        second_slot_spinner = (Spinner)myFragmentView.findViewById(R.id.second_slot_spinner);
        third_slot_spinner = (Spinner)myFragmentView.findViewById(R.id.third_slot_spinner);
        fourth_slot_spinner = (Spinner)myFragmentView.findViewById(R.id.fourth_slot_spinner);

        // Garrett change 12/15/15
        // changed this from android.r.layout.simple_spinner_layout1 (something like that)
        // to a custom layout so that we can set the size of the text
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.diningSpinner_titles,
                R.layout.spinner_text_format);
        adapter.setDropDownViewResource(R.layout.spinner_text_format);

        first_slot_spinner.setAdapter(adapter);
        second_slot_spinner.setAdapter(adapter);
        third_slot_spinner.setAdapter(adapter);
        fourth_slot_spinner.setAdapter(adapter);

        first_slot_spinner.setOnItemSelectedListener(this);
        second_slot_spinner.setOnItemSelectedListener(this);
        third_slot_spinner.setOnItemSelectedListener(this);
        fourth_slot_spinner.setOnItemSelectedListener(this);

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        savedPref = sharedPref.getBoolean("savedPref", false);

        if(savedPref){
            first_slot_spinner.setSelection(readPreference(0));
            second_slot_spinner.setSelection(readPreference(1));
            third_slot_spinner.setSelection(readPreference(2));
            fourth_slot_spinner.setSelection(readPreference(3));
        }
        else
            setDefaultPreferences();

        return myFragmentView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d("NI", "You selected something.");
        switch(parent.getId()){
            case R.id.first_slot_spinner:
                MainActivity.orderedDiningHalls[0] = diningHallIndexes[position];
                savePreference(0,position);
                break;
            case R.id.second_slot_spinner:
                MainActivity.orderedDiningHalls[1] = diningHallIndexes[position];
                savePreference(1,position);
                break;
            case R.id.third_slot_spinner:
                MainActivity.orderedDiningHalls[2] = diningHallIndexes[position];
                savePreference(2,position);
                break;
            case R.id.fourth_slot_spinner:
                MainActivity.orderedDiningHalls[3] = diningHallIndexes[position];
                savePreference(3,position);
                break;
            default:
                Log.d("NI", "You clicked: " + parent.getId());
                break;
        }
    }

    private void setDefaultPreferences(){
        savePreference(0, 0);
        first_slot_spinner.setSelection(0);
        savePreference(1, 1);
        second_slot_spinner.setSelection(1);
        savePreference(2, 2);
        third_slot_spinner.setSelection(2);
        savePreference(3, 3);
        fourth_slot_spinner.setSelection(3);
    }

    private void savePreference(int spinner, int hallSelected){
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("savedPref", true);
        switch(spinner){
            case 0:
                editor.putInt("firstSpinner", hallSelected);
                break;
            case 1:
                editor.putInt("secondSpinner", hallSelected);
                break;
            case 2:
                editor.putInt("thirdSpinner", hallSelected);
                break;
            case 3:
                editor.putInt("fourthSpinner", hallSelected);
                break;
        }
        editor.commit();
    }

    private int readPreference(int spinner){
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        int ret;
        switch(spinner){
            case 0:
                ret =  sharedPref.getInt("firstSpinner", 0);
                break;
            case 1:
                ret = sharedPref.getInt("secondSpinner", 1);
                break;
            case 2:
                ret = sharedPref.getInt("thirdSpinner", 2);
                break;
            case 3:
                ret = sharedPref.getInt("fourthSpinner", 3);
                break;
            default:
                ret = 0;
                break;
        }
        return ret;
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSettingsFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onSettingsFragmentInteraction(Uri uri);
    }



}
