package com.nikitaivanov.ucladiningassistant;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Adapts a list of dish objects for display within the Kitchens ListViews of DiningHallFragments
 */
public class DishesAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Dish> dishes;

    public DishesAdapter(Context context, ArrayList<Dish> dishes){
        this.context = context;
        this.dishes = dishes;
    }

    @Override
    public int getCount() {
        return dishes.size();
    }

    @Override
    public Object getItem(int position) {
        return dishes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Inflate layout and find various views within it
        View mView = layoutInflater.inflate(R.layout.dish_list_item, null);

        CheckBox favStar = (CheckBox)mView.findViewById(R.id.fav_star);
        TextView dishName = (TextView)mView.findViewById(R.id.dish_name);

        //Get dish from list
        Dish dish = dishes.get(position);

        //Set listener for changes to favStar
        OnStarChangeListener listener = new OnStarChangeListener(context, dish);
        favStar.setOnCheckedChangeListener(listener);

        //Set favStar as checked with the dish is contained with favorites
        if(MainActivity.favoriteDishesSet.contains(dish)){
            favStar.setChecked(true);
        }
        else
            favStar.setChecked(false);

        dishName.setText(dish.getName());

        return mView;



    }
}
