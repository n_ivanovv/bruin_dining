package com.nikitaivanov.ucladiningassistant;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Listener for the star checkbox used to manipulate favorite dishes
 * Extended class rather than using anonymous object in order to avoid requiring final variables
 */
public class OnStarChangeListener implements CompoundButton.OnCheckedChangeListener{
    private Dish dish;
    private Context context;

    OnStarChangeListener(Context context, Dish dish){
        this.context = context;
        this.dish = dish;

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            MainActivity.favoriteDishesSet.add(dish);
            //Toast.makeText(context, dish + " added to favorites.", Toast.LENGTH_SHORT).show(); BUGGY - SPAMMED (WHY?)
            buttonView.setChecked(true);
        }
        else {
            MainActivity.favoriteDishesSet.remove(dish);
            //Toast.makeText(context, dish + " removed from favorites.", Toast.LENGTH_SHORT).show(); BUGGY - SPAMMED (WHY?)
            buttonView.setChecked(false);

        }

    }
}
