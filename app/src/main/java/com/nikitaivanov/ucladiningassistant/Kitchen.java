package com.nikitaivanov.ucladiningassistant;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * A kitchen object belongs to a given mealPeriod of a given diningHall and contains a list
 * of dishes that it serves
 */
public class Kitchen {
    private String name;
    private ArrayList<Dish> dishes;

    public String getName(){
        return name;
    }

    public Kitchen() {
        dishes = new ArrayList<Dish>();
    }

    public void setName(String name){
        this.name = name;
    }

    public ArrayList<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }

    public void addDish(Dish dish){
        dishes.add(dish);
    }

    public Dish getDish(int i){
        return dishes.get(i);
    }
}
