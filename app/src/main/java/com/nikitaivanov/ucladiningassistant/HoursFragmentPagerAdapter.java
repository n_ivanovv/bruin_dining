package com.nikitaivanov.ucladiningassistant;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Garrett on 11/9/2015.
 */
public class HoursFragmentPagerAdapter extends FragmentStatePagerAdapter {

    public HoursFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        return HoursFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        // the number of mealperiods
        return 4;
    }
}
