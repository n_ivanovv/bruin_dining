package com.nikitaivanov.ucladiningassistant;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Nikita on 11/25/2015.
 */
public class SwipeCalcAdapter extends FragmentStatePagerAdapter {

    public SwipeCalcAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return MainActivity.getTabLayout().getTabCount();
    }

    @Override
    public Fragment getItem(int position) {
        return SwipeCalcFragment.newInstance(position);
    }
}
