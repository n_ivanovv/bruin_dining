package com.nikitaivanov.ucladiningassistant;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SwipeCalcFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SwipeCalcFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SwipeCalcFragment extends Fragment {

    private int displayedPlan;
    private static int[] swipesArray = new int[4];
    private TextView swipeNum;

    private OnFragmentInteractionListener mListener;

    // TODO: Rename and change types and number of parameters
    public static SwipeCalcFragment newInstance(int position) {
        SwipeCalcFragment fragment = new SwipeCalcFragment();
        Bundle args = new Bundle();
        args.putInt("displayedPlan", position);
        fragment.setArguments(args);
        return fragment;
    }

    public SwipeCalcFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            displayedPlan = getArguments().getInt("displayedPlan");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myInflatedView = inflater.inflate(R.layout.fragment_swipe_calc, container, false);

        swipeNum = (TextView)myInflatedView.findViewById(R.id.swipe_num);

        SwipeCalculator swipeCalc = new SwipeCalculator();
        swipesArray = swipeCalc.calculateAllSwipes();

        swipeNum.setText(String.valueOf(swipesArray[displayedPlan]));
        return myInflatedView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSwipeCalcFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onSwipeCalcFragmentInteraction(Uri uri);
    }

}
