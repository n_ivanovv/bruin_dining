package com.nikitaivanov.ucladiningassistant;

import android.util.Log;

import java.util.Calendar;

/**
 * Created by Nikita on 11/24/2015.
 */
public class SwipeCalculator {

    public enum Plan {FOURTEEN, NINETEEN, FOURTEENPREMIUM, NINETEENPREMIUM}

    private Calendar currentDate;
    private Calendar startDate;
    private int daysSinceMonday;
    private int weeksSinceStart;
    private final int NINETEENSTART = 202;
    private final int FOURTEENSTART = 150;

    public SwipeCalculator() {
        currentDate = Calendar.getInstance();
        //For testing
        //currentDate.set(Calendar.MONTH, Calendar.FEBRUARY);
        //currentDate.set(Calendar.DAY_OF_MONTH, 24);
        startDate = Calendar.getInstance();
        startDate.set(Calendar.MONTH, Calendar.JANUARY);
        startDate.set(Calendar.DAY_OF_MONTH, 4);


        daysSinceMonday = currentDate.get(Calendar.DAY_OF_WEEK) - Calendar.MONDAY;
        Log.d("NIS", "There have been " + daysSinceMonday + " days since Monday.");
        weeksSinceStart = ((currentDate.get(Calendar.DAY_OF_YEAR) - startDate.get(Calendar.DAY_OF_YEAR)) / 7);
        //Log.d("NI", "Day of year" + currentDate.get(Calendar.DAY_OF_YEAR) + " start date: " + startDate.get(Calendar.DAY_OF_YEAR));
        //Log.d("NI", "There have been " + weeksSinceStart + " weeks since Start.");

    }

    public int[] calculateAllSwipes() {
        int[] allPeriodsArray = new int[4];
        for (Plan plan : Plan.values()) {
            allPeriodsArray[plan.ordinal()] = calculateSwipes(plan);
        }
        return allPeriodsArray;
    }

    public int calculateSwipes(Plan plan) {
        int currentSwipes = 0;
        int periodsSinceMonday;
        if (plan == Plan.NINETEENPREMIUM || plan == Plan.NINETEEN) {
            if (currentDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                periodsSinceMonday = 17;
            } else {
                periodsSinceMonday = daysSinceMonday * 3;
            }
            periodsSinceMonday += MainActivity.getCurrentMealPeriod();
            Log.d("NIS", "There have been " + periodsSinceMonday + " periods since Monday)");
            if (plan == Plan.NINETEENPREMIUM) {
                currentSwipes = NINETEENSTART;
                if(currentDate.get(Calendar.DAY_OF_YEAR) > startDate.get(Calendar.DAY_OF_YEAR)){
                    Log.d("NIS", "There have been " + weeksSinceStart);
                    currentSwipes -= 19 * weeksSinceStart + periodsSinceMonday;

                }

            } else
                currentSwipes = 19 - periodsSinceMonday;

        } else if (plan == Plan.FOURTEENPREMIUM || plan == Plan.FOURTEEN) {
            if (currentDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                periodsSinceMonday = 12;
            } else {
                periodsSinceMonday = daysSinceMonday * 2;
            }
            periodsSinceMonday += MainActivity.getCurrentMealPeriod();
            if (plan == Plan.FOURTEENPREMIUM) {
                currentSwipes = FOURTEENSTART;
                if(currentDate.get(Calendar.DAY_OF_YEAR) > startDate.get(Calendar.DAY_OF_YEAR))
                    currentSwipes -= 14 * weeksSinceStart + periodsSinceMonday;
            } else
                currentSwipes = 14 - periodsSinceMonday;
        }

        if(plan == Plan.NINETEENPREMIUM) {
            currentSwipes += holidayOffset(plan);
            Log.d("NIS", "The offset is " + holidayOffset(plan));
        }
        if (currentSwipes < 0)
            return 0;
        else
            return currentSwipes;
    }


    private int holidayOffset(Plan plan) {
        int holidayOffset = 0;

        /*Calendar vetDay = Calendar.getInstance();
        vetDay.set(Calendar.MONTH, Calendar.NOVEMBER);
        vetDay.set(Calendar.DAY_OF_MONTH, 11);

        Calendar thanksgivingBreak = Calendar.getInstance();
        thanksgivingBreak.set(Calendar.MONTH, Calendar.NOVEMBER);
        thanksgivingBreak.set(Calendar.DAY_OF_MONTH, 29);

        if (plan == Plan.NINETEENPREMIUM) {
            if (currentDate.get(Calendar.DAY_OF_YEAR) > vetDay.get(Calendar.DAY_OF_YEAR))
                holidayOffset++;
        }
        if (plan == Plan.NINETEENPREMIUM || plan == Plan.FOURTEENPREMIUM) {
            if (currentDate.get(Calendar.DAY_OF_YEAR) >= thanksgivingBreak.get(Calendar.DAY_OF_YEAR)) {
                int swipesDay = (plan == Plan.NINETEENPREMIUM) ? 3 : 2;
                holidayOffset += swipesDay * 3;
                if (plan == Plan.FOURTEENPREMIUM)
                    //Not sure why calculation is off. Crude patch until cause is found.
                    holidayOffset += 2;
            }
        }*/



        if(plan == Plan.NINETEENPREMIUM){

            Calendar mlkDay = Calendar.getInstance();
            mlkDay.set(Calendar.MONTH, Calendar.JANUARY);
            mlkDay.set(Calendar.DAY_OF_MONTH, 18);

            Calendar presDay = Calendar.getInstance();
            presDay.set(Calendar.MONTH, Calendar.FEBRUARY);
            presDay.set(Calendar.DAY_OF_MONTH, 15);


            if(currentDate.get(Calendar.DAY_OF_YEAR) >= mlkDay.get(Calendar.DAY_OF_YEAR))
                ++holidayOffset;

            if(currentDate.get(Calendar.DAY_OF_YEAR) >= presDay.get(Calendar.DAY_OF_YEAR))
                ++holidayOffset;
        }

        return holidayOffset;
    }
}
