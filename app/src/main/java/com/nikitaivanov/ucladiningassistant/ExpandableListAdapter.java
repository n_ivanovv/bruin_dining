package com.nikitaivanov.ucladiningassistant;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Adapter used for the display of favorites dishes within expandable lists
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<String> diningHallList;
    private ArrayList<ArrayList<Dish>> favoritesList;

    public ExpandableListAdapter(Context context, ArrayList<String> dhlist, ArrayList<ArrayList<Dish>> favlist){
        this.context = context;
        this.diningHallList = dhlist;
        this.favoritesList = favlist;
    }

    @Override
    public int getGroupCount() {
        return favoritesList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return favoritesList.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return diningHallList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return favoritesList.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        //Inflate layout
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Resources res = context.getResources();
        View mView = layoutInflater.inflate(R.layout.dish_favlist_item, null);

        //Find various views
        CheckBox favStar = (CheckBox)mView.findViewById(R.id.fav_star);
        TextView dishName = (TextView)mView.findViewById(R.id.dish_name);
        TextView dishStatus = (TextView)mView.findViewById(R.id.dish_status);

        //Get appropriate dish
        Dish dish = favoritesList.get(groupPosition).get(childPosition);

        //Set views
        dishName.setText(dish.getName());
        favStar.setChecked(true);

        //Create string denoting availability of dish
        String statusText;
        if(MainActivity.todaysDishesSet.contains(dish)) {
//            String dhString = dish.getDHString();
//            String mpString = dish.getMealPeriodString();
//            statusText = res.getString(R.string.fav_status, dhString, mpString);
            statusText = dish.getMealPeriodString();
        }
        else
            statusText = "Unavailable";
        dishStatus.setText(statusText);

        //Set listener for favStar
        OnStarChangeListener listener = new OnStarChangeListener(context, dish);
        favStar.setOnCheckedChangeListener(listener);

        return mView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        //Inflate layout
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Resources res = context.getResources();
        View mView = layoutInflater.inflate(R.layout.list_group, null);

        //Set views
        String groupTitle = diningHallList.get(groupPosition);
        TextView groupHeader = (TextView)mView.findViewById(R.id.exp_list_header);
        groupHeader.setText(groupTitle);

        return mView;
    }
}
