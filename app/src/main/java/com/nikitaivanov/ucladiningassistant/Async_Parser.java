package com.nikitaivanov.ucladiningassistant;

import android.os.AsyncTask;

import com.nikitaivanov.ucladiningassistant.HoursScraper;
import com.nikitaivanov.ucladiningassistant.MainActivity;

import java.io.IOException;

public class Async_Parser extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... params) {

        try {
            MainActivity.hoursScraperList = HoursScraper.fetchDiningHours();
            DiningMenuParser menuParser = new DiningMenuParser();
            menuParser.parse();
    } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
