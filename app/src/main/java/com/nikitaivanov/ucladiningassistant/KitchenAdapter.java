package com.nikitaivanov.ucladiningassistant;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Adapts list of kitchens for display
 */
public class KitchenAdapter extends BaseAdapter implements ListAdapter {

    private Context context;
    private List<Kitchen> kitchenList;
    private int diningHall;
    private int mealPeriod;

    public KitchenAdapter(Context context, List<Kitchen> kitchenList, int diningHall, int mealPeriod) {
        this.context = context;
        this.kitchenList = kitchenList;
        this.mealPeriod = mealPeriod;
        this.diningHall = diningHall;
    }

    @Override
    // 12/15/15
    // Garrett change to prevent the UI from displaying dining halls that we dont want to be displayed.
    public int getCount() {
        if (kitchenList != null)
            return kitchenList.size();
        else
            return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Inflate layout
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.kitchen_layout, null);

        //Set name of kitchen
        TextView kitchenTitle = (TextView) view.findViewById(R.id.kitchen_title);
        kitchenTitle.setText(MainActivity.dh[diningHall].mealPeriods[mealPeriod].kitchens.get(position).getName());

        //Set up listview of kitchen dishes
        ListView kitchenDishes = (ListView) view.findViewById(R.id.kitchen_dishes);
        DishesAdapter kitchenDishesAdapter = new DishesAdapter(context, kitchenList.get(position).getDishes());

        //ArrayAdapter<String> kitchenDishesAdapter = new ArrayAdapter<String>(context, R.layout.kitchen_dishes_layout, kitchenList.get(position).getDishes());

        kitchenDishes.setDivider(null);
        kitchenDishes.setDividerHeight(0);
        kitchenDishes.setEnabled(false);
        kitchenDishes.setClickable(false);
        kitchenDishes.setAdapter(kitchenDishesAdapter);
        setListViewHeightBasedOnChildren(kitchenDishes);

        return view;
    }

    /*
        Method used to ensure that ListView of dishes does not collapse since its within another ListView
        Sets height for ListView based on the number of elements it has
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        //Accumulate height based on number of elements in list
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
            //Log.d("NI", "Total height is: " + totalHeight);
        }

        //Set height, facotring in divider height
        totalHeight += listView.getPaddingBottom() + listView.getPaddingTop();
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    @Override
    public long getItemId(int position) {
        return -1;
    }

    @Override
    public Object getItem(int position) {
        return kitchenList.get(position);
    }

}
