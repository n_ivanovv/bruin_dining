package com.nikitaivanov.ucladiningassistant;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;


/**
 * Created by Nikita Ivanov on 11/2/2015
 *
 * Displays various dining information for UCLA based on data parsed via JSoup. The information is displayed
 * using an interface which consists of fragments chosen via a Navigation Drawer. Several of these fragments
 * employ view pagers in order to display a collection of other sub-fragments.
 *
 */
public class MainActivity extends AppCompatActivity
        implements DiningFragment.OnFragmentInteractionListener, DiningHallFragment.OnFragmentInteractionListener,
        SwipeCalcFragment.OnFragmentInteractionListener, OrderFragment.OnFragmentInteractionListener {



    //Container for DiningHall Objects, which store kitchens and dishes for different meal periods
    final static int NUM_DINING_HALLS = 8;
    public static DiningHall[] dh = new DiningHall[NUM_DINING_HALLS];

    //Container for user-set ordering of DiningHalls
    final static int NUM_MENU_DINING_HALLS = 4;
    static int[] orderedDiningHalls = new int[NUM_MENU_DINING_HALLS];

    //Indexing information (used for the ordering/generation of the four fragments of a DiningFragment
    public int[] diningHallIndexes = new int[]{DiningHall.DENEVE, DiningHall.BPLATE, DiningHall.COVEL, DiningHall.FEAST};

    //HashSet for storing favorite dishes
    public static HashSet<Dish> favoriteDishesSet = new HashSet<>();
    public static HashSet<Dish> todaysDishesSet = new HashSet<>();
    final static String FAVORITES_KEY = "favorites_key";
    private static int notifId = 0;
    private final static int GROUP_NOTIF_ID = -1;


    //Convoluted way of storing hours parsing information COUGH GARRETT CHANGE THIS COUGH
    static public List<HoursScraper> hoursScraperList = new ArrayList<HoursScraper>();
    private static boolean criticalParsingError = false;

    //Timing information
    private Calendar currentDate;
    static int currentMealPeriod;

    //Nav Drawer Components
    private String[] mNavBarTitles;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    private boolean inSettings = false;


    //Tabs and related member variables
    private static TabLayout tabLayout;
    private Class currentFragClass;
    private int tabPosition;
    private int mealPeriodFragmentViewIndex = 0;
    private int swipePlanDefaultTabIndex;

    /*
        Sets-up layout elements such as the toolbar, nav drawer, and tabs; reads user preferences;
        sets menu as the default displayed fragment on start-up
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Read preferences
        readDiningFragPreference();
        readSwipePreference();
        setCurrentMealPeriod();
        loadFavorites();
        notifyFavorites();
        colorStar();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (hoursScraperList == null) {
            Toast.makeText(MainActivity.this, "Couldn't Retrieve Today's Menu", Toast.LENGTH_LONG).show();
        }

        //Set-up toolbar as actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //Set-up nav drawer
        mNavBarTitles = getResources().getStringArray(R.array.navDrawer_titles);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        nvDrawer = (NavigationView) findViewById(R.id.navView);
        setUpDrawerContent(nvDrawer);
        setupDrawer();

        //Set-up tabs
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setUpTabs(tabLayout);

        //Replace current fragment (none) with the default DiningFragment (so menu is displayed on start)
        replaceFragment(DiningFragment.newInstance(mealPeriodFragmentViewIndex));
    }

    /*
        Reads the user preferences regarding the order of the DiningHallFragments displayed by the
        Dining Fragment ViewPager. The preferences are recorded within the orderedDiningHalls array.
     */
    private void readDiningFragPreference() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        //TO DO: change to enums rather than ints
        orderedDiningHalls[0] = diningHallIndexes[sharedPref.getInt("firstSpinner", 0)];
        orderedDiningHalls[1] = diningHallIndexes[sharedPref.getInt("secondSpinner", 1)];
        orderedDiningHalls[2] = diningHallIndexes[sharedPref.getInt("thirdSpinner", 2)];
        orderedDiningHalls[3] = diningHallIndexes[sharedPref.getInt("fourthSpinner", 3)];
    }

    /*
        Save the users preference regarding their Swipe Plan into the private Shared Preferences
        @int defaultTab - the tab that has the default swipe preference
            0 - 14
            1 - 19
            2 - 14P
            3 - 19P
     */
    private void saveSwipePreference(int defaultTab) {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.default_swipe), defaultTab);
        editor.commit();
    }

    /*
        Read the users preference regarding default swipe preference and set the swipePlanDefaultTabIndex
        member variable accordingly
     */
    private void readSwipePreference() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        swipePlanDefaultTabIndex = sharedPref.getInt(getString(R.string.default_swipe), 0);
    }

    /*
        Sets listener for navigation drawer
        @param navigationView the view for which the listener is set
     */
    private void setUpDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    /*
        Setup mDrawerToggle and mDrawerLayout interaction (?)
     */
    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /*
        Replaces the fragment displayed based on the menuItem selected in the Nav Drawer
     */
    public void selectDrawerItem(MenuItem menuItem) {

        Fragment fragment = null;

        //Identify which Nav Drawer Item chosen and set currentFragClass and tab labels accordingly
        switch (menuItem.getItemId()) {
            case R.id.nav_first_fragment:
                currentFragClass = DiningFragment.class;
                tabLayout.getTabAt(0).setText("Breakfast");
                tabLayout.getTabAt(1).setText("Lunch");
                tabLayout.getTabAt(2).setText("Dinner");
                tabLayout.getTabAt(3).setText("Late Night");
                break;
            case R.id.nav_second_fragment:
                currentFragClass = HoursParentFragment.class;
                tabLayout.getTabAt(0).setText("Breakfast");
                tabLayout.getTabAt(1).setText("Lunch");
                tabLayout.getTabAt(2).setText("Dinner");
                tabLayout.getTabAt(3).setText("Late Night");
                break;
            case R.id.nav_third_fragment:
                currentFragClass = SwipeCalcFragment.class;
                tabLayout.getTabAt(0).setText("14");
                tabLayout.getTabAt(1).setText("19");
                tabLayout.getTabAt(2).setText("14P");
                tabLayout.getTabAt(3).setText("19P");
                break;
            case R.id.nav_fourth_fragment:
                tabLayout.getTabAt(0).select();
                switch(tabLayout.getSelectedTabPosition()){
                    case 0:
                        currentFragClass = OrderFragment.class;
                        break;
                    case 1:
                        currentFragClass = FavoritesFragment.class;
                        break;
                    case 2:
                    case 3:
                        currentFragClass = MiscFragment.class;
                    default:
                        break;
                }
                inSettings = true;
                tabLayout.getTabAt(0).setText("Order");
                tabLayout.getTabAt(1).setText("Favorites");
                tabLayout.getTabAt(2).setText("N/A");
                tabLayout.getTabAt(3).setText("N/A");
                break;

            default:
                currentFragClass = DiningFragment.class;
        }

        //Try to construct the appropriate fragment and catch any resulting exceptions
        //Set corresponding tab selection
        try {
            if (currentFragClass == DiningFragment.class) {
                tabLayout.getTabAt(mealPeriodFragmentViewIndex).select();
                fragment = DiningFragment.newInstance(mealPeriodFragmentViewIndex);
            } else if (currentFragClass == HoursParentFragment.class) {
                fragment = HoursParentFragment.newInstance(mealPeriodFragmentViewIndex);
                tabLayout.getTabAt(mealPeriodFragmentViewIndex).select();
                Log.d("NI", "Setting current tab to " + tabPosition);
            } else if (currentFragClass == SwipeCalcFragment.class) {
                readSwipePreference();
                fragment = SwipeCalcParentFragment.newInstance(swipePlanDefaultTabIndex);
                tabLayout.getTabAt(swipePlanDefaultTabIndex).select();
                SwipeCalcParentFragment.getPager().setCurrentItem(tabPosition);
            } else if(currentFragClass == FavoritesFragment.class){
                fragment = FavoritesFragment.newInstance();
            } else if (currentFragClass == OrderFragment.class) {
                fragment = OrderFragment.newInstance();
            } else if(currentFragClass == MiscFragment.class){
                fragment = MiscFragment.newInstance();
            }
        } catch (Exception e) {
            //e.printStackTrace();
            Log.d("NI", "Unable to create fragment for" + currentFragClass.toString());
        }

        //Replace currently displayed fragment with created fragment
        replaceFragment(fragment);

        //Set titles and close drawer
        menuItem.setChecked(true);
        toolbar.setTitle(menuItem.getTitle());
        mDrawerLayout.closeDrawers();
    }

    /*
        Replace the currently displayed fragment with the parameter fragment
        @param fragment the fragment that will replace the currently displayed fragment
     */
    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /*
        Adds and labels the default tabs to tabLayout and sets its listener
        @param tabLayout the tabLayout to which the tabs are added and the listener is set
     */
    private void setUpTabs(TabLayout tabLayout) {
        //Add tabs based on meal periods since this is the default labeling
        tabLayout.addTab(tabLayout.newTab().setText("Breakfast"));
        tabLayout.addTab(tabLayout.newTab().setText("Lunch"));
        tabLayout.addTab(tabLayout.newTab().setText("Dinner"));
        tabLayout.addTab(tabLayout.newTab().setText("Late Night"));

        //Sets  and selects the default selected tab and default fragment class
        mealPeriodFragmentViewIndex = currentMealPeriod;
        currentFragClass = DiningFragment.class;
        tabLayout.getTabAt(currentMealPeriod).select();

        //Set the tab listener
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();

                //For DiningFragment
                if (currentFragClass == DiningFragment.class) {
                    //Create a new DiningFragment based on the tab and replace the current fragment with it
                    replaceFragment(DiningFragment.newInstance(tabPosition));
                    //update to ensure continuity between Dining Menus and Hours regarding meal period
                    mealPeriodFragmentViewIndex = tabPosition;

                }
                //For HoursParentFragment
                else if (currentFragClass == HoursParentFragment.class) {
                    //Set correct fragment within pager
                    HoursParentFragment.getPager().setCurrentItem(tabPosition);
                    //update to ensure continuity between Dining Menus and Hours regarding meal period
                    mealPeriodFragmentViewIndex = tabPosition;
                }
                //For SwipeCalc Fragment
                else if (currentFragClass == SwipeCalcFragment.class) {
                    //Set correct fragment within pager
                    SwipeCalcParentFragment.getPager().setCurrentItem(tabPosition);
                    //Save the new tab as default
                    saveSwipePreference(tabPosition);
                } else if (inSettings) {
                    switch (tabPosition) {
                        case 0:
                            replaceFragment(OrderFragment.newInstance());
                            break;
                        case 1:
                            replaceFragment(FavoritesFragment.newInstance());
                            break;
                        case 2:
                            replaceFragment(MiscFragment.newInstance());
                            break;
                        case 3:
                            replaceFragment(MiscFragment.newInstance());
                            break;
                        default:
                            break;
                    }

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /*
        Public accessor for tabLayout - used by view pagers of fragments in order to synchronize
        changes between the two
        @return the tabLayout used within MainActivity
     */
    public static TabLayout getTabLayout() {
        return tabLayout;
    }

    /*
        Public accessor for list of HoursScraper objects - used by other fragments to set/display hours information
        @return the List of HoursScraper objects
     */
    public static List<HoursScraper> getHoursScraperList() {
        return hoursScraperList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home: {
                // checks to see if the Navigation Drawer is open (if closed, opens it and vice versa)
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                else
                    mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            }
            case R.id.refresh_menu: {
                //Reparse menus
                refreshing_menu();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Dummy methods to deal with fragment-interaction and satisfy implementation requirements
        //TO DO- examine how to actually utilize - possibly for tab switching?
    public void onDiningFragmentInteraction(Uri uri) {}
    public void onDiningHallFragmentInteraction(Uri uri) {}
    public void onSwipeCalcFragmentInteraction(Uri uri) {}
    public void onSettingsFragmentInteraction(Uri uri) {}

    /*
        Sets the currentMealPeriod based on the current time/date. The meal period
        varies depending on whether it is a weekend or a weekday.
     */
    private void setCurrentMealPeriod() {

        //Get Calendar info
        currentDate = Calendar.getInstance();
        int currentHour = currentDate.get(Calendar.HOUR_OF_DAY);
        int currentDay = currentDate.get(Calendar.DAY_OF_WEEK);

        //Weekday
        if (currentDay != Calendar.SUNDAY && currentDay != Calendar.SATURDAY) {
            if (currentHour < 11)
                currentMealPeriod = MealPeriod.BREAKFAST;
            else if (currentHour < 17)
                currentMealPeriod = MealPeriod.LUNCH;
            else if(currentHour < 21)
                currentMealPeriod = MealPeriod.DINNER;
            else
                currentMealPeriod = MealPeriod.LATENIGHT;
        }

        //Weekend
        else {
            if (currentHour < 17)
                currentMealPeriod = MealPeriod.LUNCH;
            else if(currentHour < 21)
                currentMealPeriod = MealPeriod.DINNER;
            else
                currentMealPeriod = MealPeriod.LATENIGHT;
        }

    }

    /*
        Accesses the current Meal Period
        @return currentMealPeriod
     */
    public static int getCurrentMealPeriod() {
        return currentMealPeriod;
    }

    /*
        Returns the proper dining hall index for the element
        @i the index (0-3) of the element within the orderedDiningHalls array
        @return the index of the dining hall element desired
     */
    public static int getDiningHallIndex(int i) {
        return orderedDiningHalls[i];
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (currentDate.DAY_OF_WEEK != Calendar.getInstance().DAY_OF_WEEK) {
            refreshing_menu();
        }
    }


    /*
        Saves user favorites to the private file "saved.data"
     */
    private void saveFavorites(){
        String filename = "saved.data";
        Log.d("NIF", "Trying to save favorites");
        try{
            FileOutputStream outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(outputStream);
            oos.writeObject(favoriteDishesSet);
            oos.close();
            Log.d("NIF", "Favorites saved.");
        } catch (Exception e){
            Log.d("NIF", "Error while saving");
            e.printStackTrace();
        }
    }

    /*
        Loads hashset of user favorites from "saved.data"
     */
    private void loadFavorites(){
        String filename = "saved.data";
        try {
            FileInputStream inputStream = openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(inputStream);
            favoriteDishesSet = (HashSet<Dish>)ois.readObject();
            Log.d("NIF", "Favorites loaded.");
        } catch (Exception e){
            Log.d("NIF", "Error while loading");
        }


    }

    /*
        Creates a push notification for a given dish object
     */
    private void createNotification(Dish dish) {
        //Construct string denoting availability time/place or dish
        StringBuilder sb = new StringBuilder();
        sb.append("Served during ");
        String mealPeriod = dish.getMealPeriodString();
        sb.append(mealPeriod);
        sb.append(" at ");
        String diningHall = dish.getDHString();
        sb.append(diningHall);
        String text = sb.toString();

        //Create notif builder object, setting icon, title, text, etc.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_local_dining_white_24dp)
                .setContentText(text)
                .setContentTitle(dish.getName())
                .setGroup(FAVORITES_KEY)
                .setSortKey(dish.getName());
        //mBuilder.build();

        //Set intent so that clicking notification launched the app
        Intent resultIntent = new Intent(this, SplashActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        Log.d("NIF", "Notification created for " + dish);

        //Build notification
        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notifId, mBuilder.build());

    }

    /*
        Creates summary notification for a group of dish objects
     */
    private void createSummaryNotification(ArrayList<Dish> dishes){
        //Create title string based on number of available dishes; set notif style to InboxStyle
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String bigText = dishes.size() + " favorites available.";
        inboxStyle.setBigContentTitle(bigText);

        Resources res = getResources();
        //Iterate through dishes, adding a line describing availability for each dish to the notification
        for(Dish dish : dishes){
            String dishName = dish.getName();
//            if(dishName.length() > 18){
//                dishName = dishName.substring(0, 17);
//                dishName += "...";
//            }
            String mealPeriod = dish.getMealPeriodString();
            String diningHall = dish.getDHString();
            //String line = String.format(res.getString(R.string.fav_messages), dishName, mealPeriod, diningHall);
            //inboxStyle.addLine(styledText);

            //Format the line, setting the dish name as bold
            String lineFormat = res.getString(R.string.fav_messages);
            int lineParamsStartPos = lineFormat.indexOf("%1$s");
            String lineFormatted = res.getString(R.string.fav_messages, dishName, mealPeriod, diningHall);
            Spannable sb = new SpannableString(lineFormatted);
            sb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), lineParamsStartPos,
                    lineParamsStartPos + dishName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            inboxStyle.addLine(sb);

        }
        //Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.notification);

        //Build notifbuilder object, specifying that it is a group summary for objects that have FAVORITES_KEY
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(bigText)
                .setContentText("Expand notification to view list.")
                .setSmallIcon(R.drawable.ic_local_dining_white_24dp)
                .setStyle(inboxStyle)
                .setGroup(FAVORITES_KEY)
                .setGroupSummary(true);


        //Set intent interactions so that clicking notification launches the app
        Intent resultIntent = new Intent(this, SplashActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        //Build the notification
        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(GROUP_NOTIF_ID, mBuilder.build());
    }

    /*
        Notifies the user of their favorites by creating a notification for each dish that is
        both a user favorite and available today; if there are multiple available dishes,
        create a summary notification
     */
    private void notifyFavorites(){
        Log.d("NIF", "Notify favorites called.");
        ArrayList<Dish> dishes = new ArrayList<>();
        for(Dish dish : favoriteDishesSet){
            if(todaysDishesSet.contains(dish)){
                Log.d("NIF", "Favorites contains " + dish.getName());
                createNotification(dish);
                ++notifId;
                dishes.add(dish);
            }
        }
        if(dishes.size() > 1)
            createSummaryNotification(dishes);
    }

    /*
        Tints the filled and unfilled star icons at launch for use in other fragments
     */
    private void colorStar(){
        Drawable starChecked = ContextCompat.getDrawable(this, R.drawable.ic_grade_white_18dp);
        starChecked.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent2), PorterDuff.Mode.SRC_ATOP);

        Drawable starUnchecked = ContextCompat.getDrawable(this, R.drawable.ic_star_border_white_18dp);
        starUnchecked.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
    }

    /*
        Mutator method that sets whether an error occured while parsing
        @param error - true if an error occured, false otherwise
     */
    public static void setParsingStatus(boolean error){
        criticalParsingError = error ;
    }

    /*
        Accessor method that describes whether a critical error occured while parsing
         @return true if an error occured, false otherwise
     */
    public static boolean getParsingStatus(){
        return criticalParsingError;
    }


    /*
        Asynchroniously parses the hours and menu sites. If the parsing does not succeed due to
        timing out or another error, displays a toast stating so
     */
    private class Async_Refresh extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            return call_parsers();
        }

        @Override
        //Checks if connection timed out while parsing and displays toast if that is the case
        protected void onPostExecute(Boolean connection_timed_out) {
            if (connection_timed_out) {
                Toast.makeText(MainActivity.this, "Couldn't Retrieve Today's Menu", Toast.LENGTH_LONG).show();
            }



        }
    }

    /*
        Calls the two parsers. If parsing successful, manipulates kitchens arraylist based on closed hours
        @return true if hours list is empty (timed out while parsing), false otherwise
     */
    public static boolean call_parsers() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        hoursScraperList = HoursScraper.fetchDiningHours();

        DiningMenuParser menuParser = new DiningMenuParser();

        try {
            menuParser.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (hoursScraperList == null) {
            return true;
        } else {

            // 12/15/15
            // Garrett change
            // basic idea:
            // Nikita has organized his information as an array of dininghall objects
            // each dining hall has a list of mealperiod objects
            // each mealperiod object has a list of kitchen objects
            // each kitchen object has a list of dishes
            // so, when we iterate through my hoursscraperlist, if we find a dining hall which is closed
            // for a certain meal period, we want to set its corresponding kitchen object in Nikitas stuff to null
            for(int i = 0; i < hoursScraperList.size() && i < dh.length; i++) {
                if(hoursScraperList.get(i).getBreakHrs()[0].contains("CLOSED")) {
                    dh[i].getMealPeriod(MealPeriod.BREAKFAST).kitchens = null;
                }
                if(hoursScraperList.get(i).getLunchHrs()[0].contains("CLOSED")) {
                    dh[i].getMealPeriod(MealPeriod.LUNCH).kitchens = null;
                }
                if(hoursScraperList.get(i).getDinnerHrs()[0].contains("CLOSED")) {
                    dh[i].getMealPeriod(MealPeriod.DINNER).kitchens= null;
                }
                if (hoursScraperList.get(i).getLateNightHrs()[0].contains("CLOSED")) {
                    dh[i].getMealPeriod(MealPeriod.LATENIGHT).kitchens = null;
                }
            }

            return false;
        }

    }

    /*
        Reparses the menus and hours by executing a new Asynch_Refresh object
     */
    public void refreshing_menu() {
        Toast.makeText(MainActivity.this, "Fetching Today's Menu", Toast.LENGTH_SHORT).show();
        new Async_Refresh().execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveFavorites();
    }
}

