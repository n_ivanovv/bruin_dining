package com.nikitaivanov.ucladiningassistant;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A fragment that displays the menu for a given Dining Hall at a given meal period
 */
public class DiningHallFragment extends Fragment {
    private ArrayList<Kitchen> kitchens;
    private DiningHall diningHall;

    private int mealPeriodDisplayed;
    private int diningHallDisplayed;

    private OnFragmentInteractionListener mListener;

    public DiningHallFragment() {}

    /*
        Creates a new DiningHallFragment
        @param diningHall the dining hall index of the dining hall displayed
        @param mealPeriod the meal period index of the meal period displayed
        @return a DiningHallFragment object based on the above parameters
     */
    public static DiningHallFragment newInstance(int diningHall, int mealPeriod) {
        //Create new DiningHallFragment
        DiningHallFragment diningHallFragment = new DiningHallFragment();

        //Set arguments for the fragment
        Bundle args = new Bundle();
        args.putInt("diningHallDisplayed", diningHall);
        args.putInt("mealPeriod", mealPeriod);
        diningHallFragment.setArguments(args);

        return diningHallFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Use retrieved arguments to set member variables
        this.diningHallDisplayed = getArguments().getInt("diningHallDisplayed");
        this.mealPeriodDisplayed = getArguments().getInt("mealPeriod");

        //Set diningHall hall based on MainActivity.dh container
        this.diningHall = MainActivity.dh[diningHallDisplayed];

        kitchens = diningHall.mealPeriods[mealPeriodDisplayed].getKitchens();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_dining_hall, container, false);

        //Dining Hall Title
        TextView title = (TextView) mView.findViewById(R.id.dining_hall_title);
        title.setText(diningHall.getName());

        //Dining hall error message - not displayed unless necessary
        TextView error_message = (TextView) mView.findViewById(R.id.dining_hall_title_error_message);
        error_message.setVisibility(View.GONE);

        //Link to menus in case parsing fails
        TextView menu_link = (TextView)mView.findViewById(R.id.menu_link);
        menu_link.setVisibility(View.GONE);

        //Dining hall error image  - not displayed unless necessary
        ImageView image = (ImageView) mView.findViewById(R.id.error_image);
        image.setVisibility(View.GONE);

        // 12/15/15
        // Garrett change for dealing with Dining Halls that should not be displayed.
        // Did not work because, for example, Covel does not display a breakfast menu
        // but is open for breakfast
//        if(MainActivity.getParsingStatus() && kitchens == null){
//            error_message.setText("(Sorry, we had trouble loading the menus.)");
//            error_message.setTextColor(getResources().getColor(R.color.error_red));
//            title.setTextColor(getResources().getColor(R.color.error_red));
//            image.setVisibility(View.VISIBLE);
//            error_message.setVisibility(View.VISIBLE);
//            String linkText = "Click <a href='http://menu.ha.ucla.edu/foodpro/default.asp'>here</a> to see the menus.";
//            menu_link.setText(Html.fromHtml(linkText));
//            menu_link.setMovementMethod(LinkMovementMethod.getInstance());
//            menu_link.setVisibility(View.VISIBLE);
//        }
        if (kitchens == null) {
            error_message.setText("(Closed)");
            title.setTextColor(getResources().getColor(R.color.error_red));
            error_message.setTextColor(getResources().getColor(R.color.error_red));

            //title.setGravity(Gravity.CENTER);
            image.setVisibility(View.VISIBLE);
            error_message.setVisibility(View.VISIBLE);
        } else {
            boolean isEmpty = true;
            for(Kitchen k : kitchens) {
                if(!(k.getDishes().isEmpty())) {
                    isEmpty = false;
                    Log.d("GK", title.getText().toString() + " is not empty");
                    break;
                }
            }

            if(isEmpty) {
                error_message.setText("(Sorry, we couldn't read the menus.)");

                //We set kitches to null so that we do not display anything for its viewpager
                kitchens = null;
                image.setVisibility(View.VISIBLE);
                error_message.setVisibility(View.VISIBLE);

                if(MainActivity.getParsingStatus()) {
                    String linkText = "Click <a href='http://menu.ha.ucla.edu/foodpro/default.asp'>here</a> to see the menus.";
                    menu_link.setText(Html.fromHtml(linkText));
                    menu_link.setMovementMethod(LinkMovementMethod.getInstance());
                    menu_link.setVisibility(View.VISIBLE);
                }
            }
        }

        //Setup list view for the displayed kitchens of the dining hall
        ListView kitchenView = (ListView) mView.findViewById(R.id.kitchens);
        kitchenView.setDivider(null);
        kitchenView.setDividerHeight(0);
        kitchenView.setEnabled(false);
        kitchenView.setClickable(false);

        KitchenAdapter kitchenAdapter = new KitchenAdapter(getActivity(), kitchens, diningHallDisplayed, mealPeriodDisplayed);
        kitchenView.setAdapter(kitchenAdapter);

        return mView;
    }

    // TO DO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onDiningHallFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onDiningHallFragmentInteraction(Uri uri);
    }

}
