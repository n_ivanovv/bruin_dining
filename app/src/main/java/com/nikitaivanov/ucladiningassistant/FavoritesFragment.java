package com.nikitaivanov.ucladiningassistant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * The FavoritesFragment is used to keep track of user favorites in one easily accessible area.
 * The fragment is broken up into four expandable lists (one for each diningHall) of favorites dishes.
 * The lists can also be cleared or updated using two buttons at the top of the fragment.
 */
public class FavoritesFragment extends Fragment {

    private HashSet<Dish> dishesSet;
    private ArrayList<String> diningHallLabels;
    private ArrayList<ArrayList<Dish>> dishesArray;

    private int[] orderedDH;

    public FavoritesFragment(){
    }

    /*
        Iterates through the array orderedDH[], which denotes user preferences for the ordering of the
        dining halls. Adds labels for each diningHall following the user preferences for order.
     */
    private void generateLabels(){
        diningHallLabels = new ArrayList<>();

        for(int dh : orderedDH){
            switch(dh){
                case DiningHall.BPLATE:
                    diningHallLabels.add("BPlate");
                    break;
                case DiningHall.COVEL:
                    diningHallLabels.add("Covel");
                    break;
                case DiningHall.DENEVE:
                    diningHallLabels.add("DeNeve");
                    break;
                case DiningHall.FEAST:
                    diningHallLabels.add("Feast");
                    break;
                default:
                    break;
            }
        }
    }

    /*
        Generates the arraylist of arraylists that contains the favorite dishes served at each diningHall
     */
    private void generateDishesArray(){
        //Create master arraylist
        dishesArray = new ArrayList<>();

        //Create sub-arraylists of dishes for each dininghall
        ArrayList<Dish> bplateArray = new ArrayList<>();
        ArrayList<Dish> covelArray = new ArrayList<>();
        ArrayList<Dish> deneveArray = new ArrayList<>();
        ArrayList<Dish> feastArray = new ArrayList<>();

        //Iterate through set of favorites dishes and add to appropriate sub-arraylist based on diningHall
        for(Dish dish : dishesSet){
            switch(dish.getDiningHall()){
                case DiningHall.COVEL:
                    covelArray.add(dish);
                    break;
                case DiningHall.BPLATE:
                    bplateArray.add(dish);
                    break;
                case DiningHall.DENEVE:
                    deneveArray.add(dish);
                    break;
                case DiningHall.FEAST:
                    feastArray.add(dish);
                    break;
            }
        }

        //Add each sub-arraylist to the master arraylist based on user preferences for order
        for(int dh : orderedDH){
            switch(dh){
                case DiningHall.BPLATE:
                    dishesArray.add(bplateArray);
                    break;
                case DiningHall.COVEL:
                    dishesArray.add(covelArray);
                    break;
                case DiningHall.DENEVE:
                    dishesArray.add(deneveArray);
                    break;
                case DiningHall.FEAST:
                    dishesArray.add(feastArray);
                    break;
                default:
                    break;
            }
        }

    }
    public static FavoritesFragment newInstance() {
        //Create new DiningHallFragment
        return new FavoritesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set favorites dishes set and ordering of diningHalls based on MainActivity
        this.dishesSet = MainActivity.favoriteDishesSet;
        this.orderedDH = MainActivity.orderedDiningHalls;

        //Generate dining hall labels and the arraylists of dishes based on the above member variables
        generateLabels();
        generateDishesArray();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate layout
        View mView = inflater.inflate(R.layout.fragment_favorites, container, false);

        //Set title text view
        TextView title = (TextView)mView.findViewById(R.id.favorites_title);
        title.setText("Favorites");

        //Set up the expandableListView of favorites
        //(it is set as final to allow for interaction within anonymous OnClickListener)
        final ExpandableListView favoritesList = (ExpandableListView)mView.findViewById(R.id.expand_fav);
        favoritesList.setDivider(null);
        favoritesList.setDividerHeight(20);
        favoritesList.setAdapter(new ExpandableListAdapter(getContext(), diningHallLabels, dishesArray));
        favoritesList.setGroupIndicator(null);

        //Set up delete button
        Button deletebtn = (Button)mView.findViewById(R.id.delete_favs_btn);
        deletebtn.setText("Delete All");
        deletebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.favoriteDishesSet.clear();
                dishesSet = MainActivity.favoriteDishesSet;
                generateDishesArray();
                favoritesList.setAdapter(new ExpandableListAdapter(getContext(), diningHallLabels, dishesArray));
                saveFavorites();
            }
        });

        //Set up update button
        Button updatebtn = (Button)mView.findViewById(R.id.update_favs_btn);
        updatebtn.setText("Update");
        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateDishesArray();
                favoritesList.setAdapter(new ExpandableListAdapter(getContext(), diningHallLabels, dishesArray));
                saveFavorites();
            }
        });


        return mView;
    }

    /*
        Method for saving favorites which is used after updating/clearing using the fragment's buttons
     */
    private void saveFavorites(){
        String filename = "saved.data";
        Log.d("NIF", "Trying to save favorites");
        try{
            FileOutputStream outputStream = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(outputStream);
            oos.writeObject(MainActivity.favoriteDishesSet);
            oos.close();
            Log.d("NIF", "Favorites saved.");
        } catch (Exception e){
            Log.d("NIF", "Error while saving");
            e.printStackTrace();
        }
    }
}
