package com.nikitaivanov.ucladiningassistant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Blank fragment used as filler while other settings fragments are created
 */
public class MiscFragment extends Fragment {
    public MiscFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_misc, container, false);

        TextView title = (TextView)mView.findViewById(R.id.misc_error_title);
        title.setText("Coming Soon");

        TextView updateLink = (TextView)mView.findViewById(R.id.update_link);

        String linkText = "Click <a href='https://play.google.com/store/apps/details?id=com.nikitaivanov.ucladiningassistant&hl=en'>here</a> to check for updates.";
        updateLink.setText(Html.fromHtml(linkText));
        updateLink.setMovementMethod(LinkMovementMethod.getInstance());

        return mView;
    }

    public static MiscFragment newInstance(){
        return new MiscFragment();
    }
}
