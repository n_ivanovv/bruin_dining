package com.nikitaivanov.ucladiningassistant;

/**
 * Created by Garrett on 11/3/2015.
 */
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//import java.time.LocalDateTime;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HoursScraper {
    private String name;
    private String[] breakfasthours = new String[2];
    private String[] lunchhours = new String[2];
    private String[] dinnerhours = new String[2];
    private String[] latenighthours = new String[2];

    public HoursScraper(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBreakHrs(String[] hrs) {
        this.breakfasthours = hrs;
    }

    public void setLunchHrs(String[] hrs) {
        this.lunchhours = hrs;
    }

    public void setDinnerHrs(String[] hrs) {
        this.dinnerhours = hrs;
    }

    public void setLateNightHrs(String[] hrs) {
        this.latenighthours = hrs;
    }

    public String getName() {
        return this.name;
    }

    public String[] getBreakHrs() {
        return this.breakfasthours;
    }

    public String[] getLunchHrs() {
        return this.lunchhours;
    }

    public String[] getDinnerHrs() {
        return this.dinnerhours;
    }

    public String[] getLateNightHrs() {
        return this.latenighthours;
    }

    public static List<HoursScraper> fetchDiningHours() {
        // url of the dining hours page
        //String url = "https://secure5.ha.ucla.edu/restauranthours/dining-hall-hours-by-day.cfm";

        // for testing
        String url = "https://secure5.ha.ucla.edu/restauranthours/dining-hall-hours-by-day.cfm";
        Log.d("GK Consulting", "Fetching " + url);

        List<HoursScraper> hoursScraperList = null;

        for(int i = 0; i < MainActivity.dh.length; i++){
            MainActivity.dh[i] = new DiningHall();
        }

        try {
            // stores the parsed webpage in doc
            Document doc = Jsoup.connect(url).timeout(0).get();

            // filters for the dining halls
            // the .not() function gets filters out the meal headings (breakfast,
            // lunch, dinner...)
            Elements halls = doc.select("td[bgcolor=#ffffd5]").not("td[width]");

            // filters for the hours
            Elements hours = doc.select("td[bgcolor=#cae4ff]");

            hoursScraperList = new ArrayList<HoursScraper>();

            // creates an array in which we store the DiningHall objects
            // HoursScraper[] dh = new HoursScraper[8];
            for (int i = 0, leng = halls.size(); i < leng; ++i) {
                // dh[i] = new HoursScraper();

                // sets the name of the object
                // dh[i].setName(halls.get(i).text());
                MainActivity.dh[i].setName(halls.get(i).text());
                hoursScraperList.add(new HoursScraper(halls.get(i).text()));
            }

            // sets all four hours for each dining hall.
            // pretty efficient but looks weird. uses int arithmetic to help get the right hours to each dining hall.
            int counter = 0;
            int leng = hours.size();
            while (counter != leng) {
                hoursScraperList.get(counter / 4).setBreakHrs(parseHoursAndComments(hours.get(counter).text()));
                ++counter;
                hoursScraperList.get(counter / 4).setLunchHrs(parseHoursAndComments(hours.get(counter).text()));
                ++counter;
                hoursScraperList.get(counter / 4).setDinnerHrs(parseHoursAndComments(hours.get(counter).text()));
                ++counter;
                hoursScraperList.get(counter / 4).setLateNightHrs(parseHoursAndComments(hours.get(counter).text()));
                ++counter;


            }
        }catch (IOException e) {
            Log.d("GK Consulting", "IOException");
        }
        return hoursScraperList;


        // LocalDateTime date = LocalDateTime.now();
        // System.out.println("\nFor: " + date.getDayOfWeek().toString() + ", " + date.getMonth().toString() + " " + date.getDayOfMonth() + " " + date.getYear());
        // System.out.println("\nFrom DiningHall array:");
    }

    private static String[] parseHoursAndComments(String string) {
        String[] strings = new String[2];

        if(string.contains("(")) {
            int index = string.indexOf("(");
            strings[0] = string.substring(0, index - 1);
            strings[1] = string.substring(index);
        } else {
            strings[0] = string;
            strings[1] = null;
        }

        return strings;
    }
}

