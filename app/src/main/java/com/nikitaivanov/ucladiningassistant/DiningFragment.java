package com.nikitaivanov.ucladiningassistant;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A fragment which consists of sub-fragments that display the menu for a given dining hall in a
 * given meal period. The fragment fragment is navigated using a view pager.
 */
public class DiningFragment extends Fragment {

    private String mActivityTitle;
    VerticalPagerAdapter mVerticalPagerAdapter;
    private VerticalViewPager mVerticalViewPager;
    private OnFragmentInteractionListener mListener;
    private View mView;
    private View mPagerView;

    //private static int currentDiningHall;
    private int mealPeriod;

    /*
        Creates a new DiningFragment based on the current meal period
        @param mealPeriod the current meal period
        @return a new DiningFragment
     */
    public static DiningFragment newInstance(int mealPeriod) {
        DiningFragment fragment = new DiningFragment();
        Bundle args = new Bundle();
        args.putInt("mealPeriod", mealPeriod);
        fragment.setArguments(args);
        Log.d("NI", "Constructed a fragment for period" + mealPeriod);
        return fragment;
    }

    public DiningFragment() {
        // Required empty public constructor

    }

    /*
    public void setCurrentDiningHall(int current){
        currentDiningHall = current;
    }

    public int getCurrentDiningHall(){
        return currentDiningHall;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mealPeriod = getArguments().getInt("mealPeriod");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_dining, container, false);
        mPagerView = mView.findViewById(R.id.pager);

        setupVerticalPaging();

        return mView;
    }

    /*
    public VerticalViewPager getPager(){
        return mVerticalViewPager;
    }*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onDiningFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * TO-DO: look into actual implementation rather than just blank dummy interfacing
     *
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onDiningFragmentInteraction(Uri uri);
    }

    /*
        Sets up vertical paging within the fragment by initializing its related member variables
     */
    private void setupVerticalPaging(){
        mVerticalPagerAdapter = new VerticalPagerAdapter(getChildFragmentManager(), mealPeriod);
        mVerticalViewPager = (VerticalViewPager) mPagerView;
        mVerticalViewPager.setAdapter(mVerticalPagerAdapter);
    }

}
