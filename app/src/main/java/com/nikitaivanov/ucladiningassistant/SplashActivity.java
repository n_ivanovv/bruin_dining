package com.nikitaivanov.ucladiningassistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by Garrett on 11/22/2015.
 */


public class SplashActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        MainActivity.call_parsers();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
