package com.nikitaivanov.ucladiningassistant;

import java.io.Serializable;

/**
 * A Dish object includes its name as well as the meal period and dining hall when/where its served.
 * The object is serializable so that it can be stored persistently in between uses of the app.
 */
public class Dish implements Serializable {
    private int diningHall;
    private int mealPeriod;
    private String name;

    Dish(String name, int diningHall, int mealPeriod){
        this.name = name;
        this.diningHall = diningHall;
        this.mealPeriod = mealPeriod;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public int getDiningHall() {
        return diningHall;
    }


    public int getMealPeriod() {
        return mealPeriod;
    }

    public String getDHString(){
        String diningHallString = "";
        switch(diningHall){
            case DiningHall.BPLATE:
                diningHallString = "BPlate";
                break;
            case DiningHall.COVEL:
                diningHallString = "Covel";
                break;
            case DiningHall.DENEVE:
                diningHallString = "De Neve";
                break;
            case DiningHall.FEAST:
                diningHallString = "Feast";
                break;
            default:
                break;
        }
        return diningHallString;
    }

    public String getMealPeriodString() {
        String mealPeriodString = "";
        switch (mealPeriod) {
            case 0:
                mealPeriodString = "Breakfast";
                break;
            case 1:
                mealPeriodString = "Lunch";
                break;
            case 2:
                mealPeriodString = "Dinner";
                break;
            default:
                break;
        }
        return mealPeriodString;
    }




        @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (diningHall != dish.diningHall) return false;
        if (mealPeriod != dish.mealPeriod) return false;
        return name.equals(dish.name);

    }

    @Override
    public int hashCode() {
        int result = diningHall;
        result = 31 * result + mealPeriod;
        result = 31 * result + name.hashCode();
        return result;
    }
}
