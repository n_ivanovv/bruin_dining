package com.nikitaivanov.ucladiningassistant;

import java.util.ArrayList;
import java.util.Vector;

/*
    An object used for record-keeping for each dining hall. A dining hall object has a name
    and four meal period objects (breakfast,lunch,dinner,late night) which contain its kitchens/meals
    for the given meal period.
 */
class DiningHall {
    private String name;
//    private String breakfasthours;
//    private String lunchhours;
//    private String dinnerhours;
//    private String latenighthours;
    public MealPeriod[] mealPeriods;

    final static int BPLATE = 1;
    final static int DENEVE = 4;
    final static int COVEL = 3;
    final static int FEAST = 6;

    public DiningHall() {
        mealPeriods = new MealPeriod[4];
        mealPeriods[MealPeriod.BREAKFAST] = new MealPeriod(MealPeriod.BREAKFAST);
        mealPeriods[MealPeriod.LUNCH] = new MealPeriod(MealPeriod.LUNCH);
        mealPeriods[MealPeriod.DINNER] = new MealPeriod(MealPeriod.DINNER);
        mealPeriods[MealPeriod.LATENIGHT] = new MealPeriod(MealPeriod.LATENIGHT);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(){ return name; }

    /*public void setBreakHrs(String hrs) {
        this.breakfasthours = hrs;
    }

    public void setLunchHrs(String hrs) {
        this.lunchhours = hrs;
    }

    public void setDinnerHrs(String hrs) {
        this.dinnerhours = hrs;
    }

    public void setLateNightHrs(String hrs) {
        this.latenighthours = hrs;
    }

    public String getName() {
        return name;
    }

    public String getBreakHrs() {
        return breakfasthours;
    }

    public String getLunchHrs() {
        return lunchhours;
    }

    public String getDinnerHrs() {
        return dinnerhours;
    }

    public String getLateNightHrs() {
        return latenighthours;
    }
    */

    public MealPeriod getMealPeriod(int i){
        return mealPeriods[i];
    }
}